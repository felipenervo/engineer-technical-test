
# Software Engineer Technical Test

## Problem Statement
You are tasked with creating a simple API for a task management system. The API should allow users to perform the following operations:
1. Create a new task
2. Retrieve all tasks
3. Update a task's status
4. Delete a task

Each task should have the following attributes:
- `id`: a unique identifier for the task
- `title`: a string representing the title of the task
- `description`: a string representing the task's description
- `status`: a string representing the task's status (e.g., "pending", "in-progress", "completed")
- `created_at`: a timestamp indicating when the task was created
- `updated_at`: a timestamp indicating when the task was last updated

## Requirements
1. Implement the API using TypeScript and Node.js.
2. Use a database to store the tasks. You can choose the database engine.
3. Containerize the application using Docker and provide a `docker-compose` file to set up the database and the application.
4. Write basic tests to ensure the functionality of the API endpoints (optional/depending on the time left).

## Instructions
1. Clone this repository to your local machine.
2. Implement the required features.
3. Ensure your solution works by running the application and testing the endpoints (you can use the api.http file - it requires the http client extension installed on VSCode).

## Directory Structure
This is just a suggestion.

```
src/
  ├── controllers/
  ├── models/
  ├── routes/
  ├── services/
  ├── index.ts
docker-compose.yml
Dockerfile
README.md
```

## Time Limit
You have 45 minutes to complete this task. Focus on delivering a functional solution rather than a perfect one. Good luck!

## Starting Point
To help you get started, a basic project structure and some initial files have been provided.

---

## Project Setup

### Prerequisites
- Node.js (>=14.x)
- Docker (>=20.x)
- Docker Compose (>=1.27.x)

### Steps
1. Install dependencies:
   ```bash
   npm install
   ```

2. Start the application using Docker Compose (requires finishing the Dockerfile):
   ```bash
   docker-compose up --build
   ```

3. The API should be accessible at `http://localhost:3000`.

### Endpoints
- `POST /tasks`: Create a new task
- `GET /tasks`: Retrieve all tasks
- `PUT /tasks/:id`: Update a task's status
- `DELETE /tasks/:id`: Delete a task

Feel free to ask any questions if something is unclear. Good luck!
