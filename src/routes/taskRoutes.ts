
import { Router } from 'express';
import { TasksController } from '../controllers/tasksController';

const router = Router();
const tasksController = new TasksController();

router.post('/tasks', tasksController.createTask);
router.get('/tasks', tasksController.getTasks);
router.put('/tasks/:id', tasksController.updateTask);
router.delete('/tasks/:id', tasksController.deleteTask);

export default router;
