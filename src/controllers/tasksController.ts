import { Request, Response } from 'express';

export class TasksController {
  constructor() {}

  public createTask = async (req: Request, res: Response): Promise<void> => {
    throw new Error('Not implemented');
  };

  public getTasks = async (req: Request, res: Response): Promise<void> => {
    throw new Error('Not implemented');
  };

  public updateTask = async (req: Request, res: Response): Promise<void> => {
    throw new Error('Not implemented');
  };

  public deleteTask = async (req: Request, res: Response): Promise<void> => {
    throw new Error('Not implemented');
  };
}
